---
title: RunTime<一> Class 结构解析
date: 2017-03-15 20:17:05
tags:
---
NsObject 是iOS 中为数不多的基类之一（目前知道的另外一个基类是NsProxy），打开工程，在NSObject.h 文件中可以发现 NSObject 定义是这样的：

{% codeblock lang:objc %}
@interface NSObject <NSObject> {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-interface-ivars"
Class isa  OBJC_ISA_AVAILABILITY;
#pragma clang diagnostic pop
}
+ (void)load;
+ (void)initialize;
- (instancetype)init
...
@end
{% endcodeblock %}
![block 示例图] (/upload/block-example.png)
isa 是一个class 结构（注意没有用* ）。 选中Class ->右击->在展开的菜单中选择 "jump to definiction" ，这个时候会跳转到objc.h文件，其中class 是这样的一个 struct：
<!--more-->
{% codeblock lang:c %}
  typedef struct objc_class *Class;
{% endcodeblock %}

这个时候在选中objc_class-> 右击-> jump to definiction 这个时候会跳转到runtime.h 文件中，runtime 文件详细的描述了class 的具体结构如下：

{% codeblock lang:c %}
struct objc_class {
    Class _Nonnull isa  OBJC_ISA_AVAILABILITY;

#if !__OBJC2__
    Class _Nullable super_class                              OBJC2_UNAVAILABLE;
    const char * _Nonnull name                               OBJC2_UNAVAILABLE;
    long version                                             OBJC2_UNAVAILABLE;
    long info                                                OBJC2_UNAVAILABLE;
    long instance_size                                       OBJC2_UNAVAILABLE;
    struct objc_ivar_list * _Nullable ivars                  OBJC2_UNAVAILABLE;
    struct objc_method_list * _Nullable * _Nullable methodLists                    OBJC2_UNAVAILABLE;
    struct objc_cache * _Nonnull cache                       OBJC2_UNAVAILABLE;
    struct objc_protocol_list * _Nullable protocols          OBJC2_UNAVAILABLE;
#endif
} OBJC2_UNAVAILABLE;

{% endcodeblock %}

上面是Class 的基本结构，从runtime.h文件可以找到一些有用的函数如下：
** 1.成员变量相关函数 **
{% codeblock lang:c %}
// 获取实例成员变量
Ivar class_getInstanceVariable(Class _Nullable cls, const char * _Nonnull name);

// 获取类成员变量
Ivar class_getClassVariable(Class _Nullable cls, const char * _Nonnull name) ;

// 添加成员变量
BOOL class_addIvar(Class _Nullable cls, const char * _Nonnull name, size_t size, 
              uint8_t alignment, const char * _Nullable types) ;

// 获取整个成员变量列表
Ivar  class_copyIvarList(Class _Nullable cls, unsigned int * _Nullable outCount);

{% endcodeblock %}

**2.属性相关函数**
{% codeblock lang:c %}
// 获取指定属性
objc_property_t _Nullable class_getProperty(Class _Nullable cls, const char * _Nonnull name)

// 获取属性列表
objc_property_t _Nonnull * _Nullable class_copyPropertyList(Class _Nullable cls, unsigned int * _Nullable outCount;

// 添加属性
BOOL class_addProperty(Class _Nullable cls, const char * _Nonnull name,const objc_property_attribute_t * _Nullable attributes,unsigned int attributeCount);

// 替换属性
void class_replaceProperty(Class _Nullable cls, const char * _Nonnull name,const objc_property_attribute_t * _Nullable attributes,unsigned int attributeCount);


{% endcodeblock %}

**3.成员方法相关**
{% codeblock lang:c %}
// 添加方法
BOOL
class_addMethod(Class _Nullable cls, SEL _Nonnull name, IMP _Nonnull imp, const char * _Nullable types) ;

// 获取实例方法
 Method _Nullable class_getInstanceMethod(Class _Nullable cls, SEL _Nonnull name);

// 获取类方法
Method Method _Nullable class_getClassMethod(Class _Nullable cls, SEL _Nonnull name)( Class cls, SEL name );

// 获取所有方法的数组
Method _Nonnull * _Nullable class_copyMethodList(Class _Nullable cls, unsigned int * _Nullable outCount) ;

// 替代方法的实现
IMP class_replaceMethod(Class _Nullable cls, SEL _Nonnull name, IMP _Nonnull imp,  const char * _Nullable types) ;

// 返回方法的具体实现
IMP class_getMethodImplementation(Class _Nullable cls, SEL _Nonnull name) ;

IMP class_getMethodImplementation_stret(Class _Nullable cls, SEL _Nonnull name) ;

// 类实例是否响应指定的selector
BOOL  class_respondsToSelector(Class _Nullable cls, SEL _Nonnull sel) ;
{% endcodeblock %}

**4.协议相关方法**
{% codeblock lang:c %}
// 添加协议
BOOL  class_addProtocol(Class _Nullable cls, Protocol * _Nonnull protocol) ;

//判断类是否实现指定的协议
BOOL class_conformsToProtocol(Class _Nullable cls, Protocol * _Nullable protocol) ;

// 返回类实现的协议列表
Protocol * __unsafe_unretained _Nonnull * _Nullable class_copyProtocolList(Class _Nullable cls, unsigned int * _Nullable outCount);

{% endcodeblock %}

