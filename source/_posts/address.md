---
title: iOS的内存区域划分
date: 2017-01-24 18:59:01
tags:
---
RAM(radom access memeory)是运行内存的简写，就是常说的内存，CPU可以直接访问。
优点：读写快。
缺点：价格高，掉电不可以存储。

内存划分为五个区域
* 栈区 ：存放局部变量，临时变量，内存管理由编译器自动管理
* 堆区 ：存放程序员手动创建的对象，内存释放由程序员控制。
* 全局段：全局变量和静态变量存放在这个区域。内存分配，释放由系统管理。
* 常量段：常量存放区域，内存分配，释放由系统管理。
* 代码段：可执行代码，系统管理。

<!--more-->
下面是从低地址，高地址和应存储区域的对应关系

![dizhi](http://oapiyyuxw.bkt.clouddn.com/memory_addressjpg)

<!--more-->

{% codeblock lang:objc %}

// 一个简单的例子
static int   global_data; //全局段

@interface ViewController ()
{
    UIView *rootView; // 堆区
}
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // tempString指针 是在栈区，123456 是在常量段
    NSString *temString =@"123456"; 
}

{% endcodeblock %}
