---
title: iOS 设备相关参数一览
date: 2017-10-05 23:58:06
tags: 基础知识
---

## iOS设备相关参数速查表

| 设备名称       | 设备尺寸(inch) | UIKit Size (pt) | Native Resolution（px） | native scale | UIKit scale |
|:---------------|:-----|:---------------|:-----------------|:-------------|:------------|
| iPhone X       | 5.8  | 375 x 812      | 1125 x 2436      | 3.0          | 3.0         |
| iPhone 8 Plus  | 5.5  | 414 x 736      | 1080 x 1920      | 2.608        | 3.0         |
| iPhone 8       | 4.7  | 375 x 667      | 750 x 1334       | 2.0          | 2.0         |
| iPhone 7 Plus  | 5.5  | 414 x 736      | 1080 x 1920      | 2.608        | 3.0         |
| iPhone 6s Plus | 5.5  | 375 x 667      | 1080 x 1920      | 2.608        | 3.0         |
| iPhone 6 Plus  | 5.5  | 375 x 667      | 1080 x 1920      | 2.608        | 3.0         |
| iPhone 7       | 4.7  | 375 x 667      | 750 x 1334       | 2.0          | 2.0         |
| iPhone 6s      | 4.7  | 375 x 667      | 750 x 1334       | 2.0          | 2.0         |
| iPhone 6       | 4.7  | 375 x 667      | 750 x 1334       | 2.0          | 2.0         |
| iPhone SE      | 4.0  | 320 x 568      | 640 x 1136       | 2.0          | 2.0         |
| iPad Pro(2nd)  | 12.9 | 1024 x 1366    | 2048 x 2732      | 2.0          | 2.0         |
| iPad Pro       | 10.5 | 1112 x 834     | 2224 x 1668      | 2.0          | 2.0         |
| iPad Pro       | 12.9 | 1024 x 1366    | 2048 x 2732      | 2.0          | 2.0         |
| iPad Pro       | 9.7  | 768 x 1024     | 1536 x 2048      | 2.0          | 2.0         |
| iPad Air 2     | 9.7  | 768 x 1024     | 1536 x 2048      | 2.0          | 2.0         |
| iPad Mini 4    | 7.9  | 768 x 1024     | 1536 x 2048      | 2.0          | 2.0         |

<!--more-->
## pt VS px
* pt point ，印刷行业常用的单位等于1/72 inch，自然界标准长度单位，也称绝对长度
* px pixel , 像素，是屏幕显示的最基本的单位，是个相对大小。一个点可大可小，相同区域内点越小越清晰反之则模糊，这就是传说中的高分辨率和低分辨率。

<!--more-->

屏幕效果 VS 打印效果
在APP的使用过程中，所有的大小的概念都是基于手机屏幕，图片的大小会随着分辨的变化而变化。
在SE上大小为100*100 px 的图片到 iPhone X 上有两种方案，1）每个绝对单位内显示的像素数量不变，这样会发现原来的区域变成了33*33 左右大小，这个肯定不是想要的结果；2） 把100*100 px 的图片拉伸3倍，显示到设备上，这个时候单位面积内的像素少了，所以画面会变模糊。
通过对比发现苹果采用的是方案二，通过给资源文件加上后缀 @2x,@3x 来适配不同分辨率的手机。

## 图片的显示策略
在iOS系统中，内容展示的基本计量单位是pt,通过将px映射到pt的坐标系中，这样对于不同分辨率的设备单位pt中的px的数量是不一样的。
![2x，3x，图片显示](http://oapiyyuxw.bkt.clouddn.com/ImageResolution-Graphic_2x.png)

## 常用icon大小统计
* iPhone app Icon 大小一览
![iPhone app Icon 大小一览](http://oapiyyuxw.bkt.clouddn.com/屏幕快照 2018-08-10 下午5.46.50.png)
* iPad app Icon 大小一览
![iPad app Icon 大小一览](http://oapiyyuxw.bkt.clouddn.com/屏幕快照 2018-08-10 下午5.47.06.png)

## 参考
[①：iOS Device Compatibility Reference](https://developer.apple.com/library/archive/documentation/DeviceInformation/Reference/iOSDeviceCompatibility/Displays/Displays.html)
[②：PT与PX区别](https://www.douban.com/note/155032221/)
[③：iOS设计规范之image Size](https://developer.apple.com/design/human-interface-guidelines/ios/icons-and-images/image-size-and-resolution/)
