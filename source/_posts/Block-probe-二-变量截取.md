---
title: Block probe <二> 变量捕获
date: 2018-04-11 16:42:06
tags:
---
上一篇文章给出的blcok 的简单定义，带有自动变量（局部变量）的匿名函数，那我们就通过一个简单的例子来研究下变量的捕获的实现，OC 中的变量有全局变量，静态全局变量，局部变量，变量还分为基础数据类型和OC对象类型的变量，为了全面了解block 对不同的变量的处理方式，特设计了下面的测试demo

{% codeblock lang:objc %}
#import "BlockCapture.h"
// ⑤静态全局变量
static int static_global_int = 50;
// ⑥全局变量，基础数据类型
int global_int = 100;
// ⑦全局变量，对象数据类型
NSObject * global_objc_var;
@implementation BlockCapture
- (void)testBlock
{
    // ①基础数据类型，局部变量
    int local_var_use = 1;
    // ②基础数据类型，局部变量,block 内部未使用
    int local_var_unuse = 2;
    // ③基础数据类型，局部静态变量
    static int static_loacl_var =2;
    // ④对象类型 局部变量
    NSObject *objcVar = [[NSObject alloc] init];

    void(^blk)(void) = ^{
        local_var_use;
        static_loacl_var;
        objcVar;

        global_objc_var;
        global_int;
        static_loacl_var;
    };
}
@end
{% endcodeblock %}
转换后的cpp 文件后的代码
<!--more-->
{% codeblock lang:objc %}
// 全局数据
// ⑤静态全局变量
static int static_global_int = 50;
// ⑥全局变量，基础数据类型
int global_int = 100;
// ⑦全局变量，对象数据类型
NSObject * global_objc_var;
// @implementation BlockCapture
struct __BlockCapture__testBlock_block_impl_0 {
  struct __block_impl impl;
  struct __BlockCapture__testBlock_block_desc_0* Desc;
  // 在struct 内部生成的同名变量（截获的实现）
  // ①基础据类型，局部变量
  int local_var_use;
  // ③基础数据类型，局部静态变量
  int *static_loacl_var; // 静态变量，生成的一个指针指向静态变量
  
  // ④对象类型 局部变量
  NSObject *objcVar;
  
  // 通过构造函数将值将局部变量传入结构体中
  __BlockCapture__testBlock_block_impl_0(void *fp, struct __BlockCapture__testBlock_block_desc_0 *desc, int _local_var_use, int *_static_loacl_var, NSObject *_objcVar, int flags=0) : local_var_use(_local_var_use), static_loacl_var(_static_loacl_var), objcVar(_objcVar) {
    impl.isa = &_NSConcreteStackBlock;
    impl.Flags = flags;
    impl.FuncPtr = fp;
    Desc = desc;
  }
};
//
static void __BlockCapture__testBlock_block_func_0(struct __BlockCapture__testBlock_block_impl_0 *__cself) {
  // 函数内部也是先从 __cself （__BlockCapture__testBlock_block_impl_0）获取被捕获对象，在赋值给匿名函数内的同名局部对象
  int local_var_use = __cself->local_var_use; // bound by copy
  int *static_loacl_var = __cself->static_loacl_var; // bound by copy
  NSObject *objcVar = __cself->objcVar; // bound by copy

        local_var_use;
        (*static_loacl_var);
        objcVar;

        global_objc_var;
        global_int;
        global_objc_var;
    }

 // OC对象还会生成额外的静态方法 copy  
static void __BlockCapture__testBlock_block_copy_0(struct __BlockCapture__testBlock_block_impl_0*dst, struct __BlockCapture__testBlock_block_impl_0*src) {_Block_object_assign((void*)&dst->objcVar, (void*)src->objcVar, 3/*BLOCK_FIELD_IS_OBJECT*/);}


 // OC对象还会生成额外的静态方法 dispose
static void __BlockCapture__testBlock_block_dispose_0(struct __BlockCapture__testBlock_block_impl_0*src) {_Block_object_dispose((void*)src->objcVar, 3/*BLOCK_FIELD_IS_OBJECT*/);}

// desc 的结构变得更复杂了
static struct __BlockCapture__testBlock_block_desc_0 {
  size_t reserved;
  size_t Block_size;
  void (*copy)(struct __BlockCapture__testBlock_block_impl_0*, struct __BlockCapture__testBlock_block_impl_0*);
  void (*dispose)(struct __BlockCapture__testBlock_block_impl_0*);
} __BlockCapture__testBlock_block_desc_0_DATA = { 0, sizeof(struct __BlockCapture__testBlock_block_impl_0), __BlockCapture__testBlock_block_copy_0, __BlockCapture__testBlock_block_dispose_0};

// 原函数的实现
static void _I_BlockCapture_testBlock(BlockCapture * self, SEL _cmd) {
    int local_var_use = 1;
     // ②基础数据类型，局部变量,block 内部未使
    int local_var_unuse = 2;
    static int static_loacl_var =2;

    NSObject *objcVar = ((NSObject *(*)(id, SEL))(void *)objc_msgSend)((id)((NSObject *(*)(id, SEL))(void *)objc_msgSend)((id)objc_getClass("NSObject"), sel_registerName("alloc")), sel_registerName("init"));

    void(*blk)(void) = ((void (*)())&__BlockCapture__testBlock_block_impl_0((void *)__BlockCapture__testBlock_block_func_0, &__BlockCapture__testBlock_block_desc_0_DATA, local_var_use, &static_loacl_var, objcVar, 570425344));
}
{% endcodeblock %}

 通过上面的代码可以得出以下结论：
   * 变量① vs 变量② ： 只有被block 用到的变量才会被捕获到block内部
   * 变量① vs 变量⑥ ： 局部变量被捕获，全局变量不需要捕获到blcok 内部
   * 变量① vs 变量③ ： 静态局部变量在捕获的过程是传递的指针值，普通的局部变量是值传递。
   * 变量① vs 变量④ ： 基础数据类型在被block 结构体捕获的过程中不仅仅是捕获变量，还会生成copy,和 dispose 函数
   * 变量①③④ vs ⑤⑥⑦  全局变量不需要被捕获到blcok 的内部。局部变量基础数据类型和对象类型捕获到内部的实现也不一样。


对原来代码做些改造，来验证上面的结论
{% codeblock lang:objc %}
#import "BlockCapture.h"
// ⑤静态全局变量
static int static_global_int = 50;
// ⑥全局变量，基础数据类型
int global_int = 100;
// ⑦全局变量，对象数据类型
NSObject * global_objc_var = nil;

@implementation BlockCapture

- (void)testBlock
{
    // ①基础数据类型，局部变量
    int local_var_use = 1;
    // ②基础数据类型，局部变量,block 内部未使用
    int local_var_unuse = 2;
    // ③基础数据类型，局部静态变量
    static int static_loacl_var =2;
    // ④对象类型 局部变量
    NSObject *objcVar = [[NSObject alloc] init];
    
    NSLog(@"block初始化前：local_var_use 内存地址 %p",&local_var_use);
    NSLog(@"block初始化前：static_loacl_var 内存地址 %p",&static_loacl_var);
    NSLog(@"block初始化前：objcVar 内存地址 %p",&objcVar);
    NSLog(@"block初始化前：global_objc_var 内存地址 %p",&global_objc_var);
    NSLog(@"block初始化前：global_objc_var 内存地址 %p",&global_int);
    NSLog(@"block初始化前：global_objc_var %p",&global_objc_var);
    NSLog(@"-----------------------------------------------------------");
    
    void(^blk)(void) = ^{
        
        NSLog(@"block：local_var_use 内存地址 %p",&local_var_use);
        NSLog(@"block：static_loacl_var 内存地址 %p",&static_loacl_var);
        NSLog(@"block：objcVar 内存地址 %p",&objcVar);
        
        NSLog(@"block：global_objc_var 内存地址 %p",&global_objc_var);
        NSLog(@"block：global_objc_var 内存地址 %p",&global_int);
        NSLog(@"block：global_objc_var %p",&global_objc_var);
    };
    blk();
    
    NSLog(@"-----------------------------------------------------------");
    NSLog(@"block执行后：local_var_use 内存地址 %p",&local_var_use);
    NSLog(@"block执行后：static_loacl_var 内存地址 %p",&static_loacl_var);
    NSLog(@"block执行后：objcVar 内存地址 %p",&objcVar);
    NSLog(@"block执行后：global_objc_var 内存地址 %p",&global_objc_var);
    NSLog(@"block执行后：global_objc_var 内存地址 %p",&global_int);
    NSLog(@"block执行后：global_objc_var %p",&global_objc_var);
}
@end
{% endcodeblock %}

结果输出
{% codeblock lang:objc %}
block初始化前：local_var_use 内存地址 0x7ffee884803c
block初始化前：static_loacl_var 内存地址 0x1073ba2bc
 block初始化前：objcVar 内存地址 0x7ffee8848030
 block初始化前：global_objc_var 内存地址 0x1073ba380
 block初始化前：global_objc_var 内存地址 0x1073ba2b8
 block初始化前：global_objc_var 0x1073ba380
-----------------------------------------------------------
 block：local_var_use 内存地址 0x600002d415b8
 block：static_loacl_var 内存地址 0x1073ba2bc
 block：objcVar 内存地址 0x600002d415b0
 block：global_objc_var 内存地址 0x1073ba380
 block：global_objc_var 内存地址 0x1073ba2b8
-----------------------------------------------------------
 block执行后：local_var_use 内存地址 0x7ffee884803c
 block执行后：static_loacl_var 内存地址 0x1073ba2bc
 block执行后：objcVar 内存地址 0x7ffee8848030
 block执行后：global_objc_var 内存地址 0x1073ba380
 block执行后：global_objc_var 内存地址 0x1073ba2b8
 block执行后：global_objc_var 0x1073ba380

{% endcodeblock %}

可以看到block 代码中局部变量的local_var_use ，objcVar 的地址和外面是不一样的，完全是blcok的障眼法。static_loacl_var 是因为传递的地址，所以前后地址一致

总结：本文主要对不同的变量捕获做了对比，得到了一下的结论：
> 1）只有被blcok 内部使用的局部变量才会被block捕获
  2）全局变量不需要被捕获，可以被block 直接访问
  3）基础数据类型和对象在blcok 的捕获的实现不一样的
  4）block 内部访问的被捕获的局部变量和外部只是同名而已，其实已不再是同一个对象。

