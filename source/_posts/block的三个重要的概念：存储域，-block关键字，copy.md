---
title: block<三> :存储域，_ _block，block_copy
date: 2018-04-11 20:13:25
tags:
---
## block 的存储域
{% asset_img block-block_stack.jpeg Block存储域 %}
通过前面几篇文章的学习，我们知道blcok 是个objective-C对象，在存储域上分为以下三种类型
* __NSStackBlock__，
* __NSMallocBlock__，
* __NSGlobalBlock__。

`blcok 作为全局变量时 或者 block 函数中没有使用截获的自动变量时的block 的类型是GlobalBlock,剩下的都是 StackBlock，当把一个block 赋值给另外一个blcok 时会在ARC下回发生copy 操作，产生的block 的类型是MallocBlock`

{% codeblock lang:objc %}
#import "BlockType.h"
@implementation BlockType

-(void)testBlockType
{
int i = 10;
void (^blk)(void) = ^{i;};
void (^globalBlk)(void) = ^{NSLog(@"hello block");};
__weak void(^weakBlk)(void) = ^{i;};
// 对比实验组1
NSLog(@"1-1 简单的blcok %@",^{NSLog(@"hello block");});
NSLog(@"1-2 globalBlk  %@",globalBlk);
// 对比实验组2
NSLog(@"2-1 直接输出 :%@",^{i;});
NSLog(@"2-2 先赋值，在输出blk :%@",blk);
// 对比实验3
NSLog(@"3-1 weakBlk :%@",weakBlk);
}
{% endcodeblock %}
<!--more-->

Block 在ARC和MRC下的机制还是不一样的，我们能从下面这份输出结果得到上述结论
ARC的输出结果
{% codeblock lang:objc %}
1-1 简单的blcok <__NSGlobalBlock__: 0x10923a0f8>
1-2 globalBlk  <__NSGlobalBlock__: 0x10923a0d8>
2-1 直接输出 :<__NSStackBlock__: 0x7ffee69c6de0>
2-2 先赋值，在输出blk :<__NSMallocBlock__: 0x6000033680c0>
3-1 weakBlk :<__NSStackBlock__: 0x7ffee69c6e10>
{% endcodeblock %}

MRC下输出结果
{% codeblock lang:objc %}
1-1 简单的blcok <__NSGlobalBlock__: 0x10aabe0e0>
1-2 globalBlk  <__NSGlobalBlock__: 0x10aabe0c0>
2-1 直接输出 :<__NSStackBlock__: 0x7ffee5142df0>
2-2 先赋值，在输出blk :<__NSStackBlock__: 0x7ffee5142e58>
3-1 weakBlk :<__NSStackBlock__: 0x7ffee5142e20>
{% endcodeblock %}

* 在上面提到了手动创建的block要么是stackBlock,要么是GlobalBlock，那么问题来了，stackblock也是有作用域的，一旦超出作用域会怎样呢？block 提供了将block 和 __block 从栈上复制到堆上的方法解决这个问题。

我们来看下2-1 和 2-2 ，其实block 的内容是一样的，2-2 有一次赋值的过程发生了block 的copy 操作所以一个blcok 是NSStackBlock，一个是NSMallocBlock，在mrc 下两个输出都是 NSStackBlock。
> 这个时候如果调用 blk（）,会发生什么情况？

## _ _ block 修饰符

为了更好的研究 _ _block修饰符对block结构的影响，我们将分别对基础数据，OC对象两种数据类型设计对比实验

|   数据类型 | 备注 |
| :----------:|:-----:|
| 基础数据类型 |  × |
| OC对象类型  |  × |
| 基础数据类型 |  ✔ |
| OC对象类型  |  ✔ |

### 测试源码

{% codeblock lang:objc  不使用_ _ block 修饰auto变量%}
#import "NotUsePre.h"
@implementation NotUsePre
-(void)testBlockCopy
{
     int i = 20;
     NSObject *objctTest = [[NSObject alloc] init];
    
    void(^blk)(void)= ^{
        i;
        objctTest;
    };
    blk();
}
@end
{% endcodeblock %}

{% codeblock lang:objc  使用_ _ block 修饰auto变量%}
#import "UseBlockPre.h"
@implementation UseBlockPre
-(void)testBlockCopy
{
    __block int i = 20;
    __block NSObject *objctTest = [[NSObject alloc] init];
    
    void(^blk)(void)= ^{
        i = 22;
        objctTest = nil;
    };
    blk();
}
@end
{% endcodeblock %}
使用clang -rewrite-objc  命令将源文件转换后，我们分别对照两组源码，比较 _ _blcok 修饰符对blcok 结构体产生的影响

### block 结构体对比

{% codeblock lang:objc  不带block修饰符的变量%}
struct __NotUsePre__testBlockCopy_block_impl_0 {
  struct __block_impl impl;
  struct __NotUsePre__testBlockCopy_block_desc_0* Desc;
  int i;
  NSObject *objctTest;
  __NotUsePre__testBlockCopy_block_impl_0(void *fp, struct __NotUsePre__testBlockCopy_block_desc_0 *desc, int _i, NSObject *_objctTest, int flags=0) : i(_i), objctTest(_objctTest) {
    impl.isa = &_NSConcreteStackBlock;
    impl.Flags = flags;
    impl.FuncPtr = fp;
    Desc = desc;
  }
};
{% endcodeblock %}

{% codeblock lang:objc  带block修饰符的impl结构体%}
struct __Block_byref_i_0 {
  void *__isa;
__Block_byref_i_0 *__forwarding;
 int __flags;
 int __size;
 int i;
};
struct __Block_byref_objctTest_1 {
  void *__isa;
__Block_byref_objctTest_1 *__forwarding;
 int __flags;
 int __size;
 void (*__Block_byref_id_object_copy)(void*, void*);
 void (*__Block_byref_id_object_dispose)(void*);
 NSObject *objctTest;
};

struct __UseBlockPre__testBlockCopy_block_impl_0 {
  struct __block_impl impl;
  struct __UseBlockPre__testBlockCopy_block_desc_0* Desc;
  __Block_byref_i_0 *i; // by ref
  __Block_byref_objctTest_1 *objctTest; // by ref
  __UseBlockPre__testBlockCopy_block_impl_0(void *fp, struct __UseBlockPre__testBlockCopy_block_desc_0 *desc, __Block_byref_i_0 *_i, __Block_byref_objctTest_1 *_objctTest, int flags=0) : i(_i->__forwarding), objctTest(_objctTest->__forwarding) {
    impl.isa = &_NSConcreteStackBlock;
    impl.Flags = flags;
    impl.FuncPtr = fp;
    Desc = desc;
  }
};
{% endcodeblock %}

对于block内使用到的变量，blcok_impl_0结构体会生成和外部同名的的结构体成员变量
* 在未使用_ _block 修饰变量时，结构体内的成员变量的类型和结构体外一样的结构，int--int，nsobject-nsobject
* 在使用_ _block 修饰变量后，基础数据类型int,对象类型NSobject 都会嵌入到 __Block_byref_ 的对象中，然后blcok_impl_0 中在写入同名的成员变量。`i变量使用__Block 修饰后，在block 内部访问的i 实际已经是一个__Block_byref结构体`


### 匿名函数内部实现对比
{% codeblock lang:objc  不带block修饰符的变量%}
static void __NotUsePre__testBlockCopy_block_func_0(struct __NotUsePre__testBlockCopy_block_impl_0 *__cself) {
  int i = __cself->i; // bound by copy
  NSObject *objctTest = __cself->objctTest; // bound by copy

        i;
        objctTest;
    }
{% endcodeblock %}


{% codeblock lang:objc  带block修饰符的变量%}
static void __UseBlockPre__testBlockCopy_block_func_0(struct __UseBlockPre__testBlockCopy_block_impl_0 *__cself) {
  __Block_byref_i_0 *i = __cself->i; // bound by ref
  __Block_byref_objctTest_1 *objctTest = __cself->objctTest; // bound by ref

        (i->__forwarding->i) = 22;
        (objctTest->__forwarding->objctTest) = __null;
    }
{% endcodeblock %}
对于没有使用_ _block 修饰的变量，在block 内部修改时，编译时会报错，
 `(i->__forwarding->i) = 22;` block 内部对变量修改是通过__forwarding 找到结构体内的i变量 赋值，__forwarding在blcok copy 中扮演重要的角色，后面在分析。

### desc 的对比

{% codeblock lang:objc  不带block修饰符的变量%}

static void __NotUsePre__testBlockCopy_block_copy_0(struct __NotUsePre__testBlockCopy_block_impl_0*dst, struct __NotUsePre__testBlockCopy_block_impl_0*src) {_Block_object_assign((void*)&dst->objctTest, (void*)src->objctTest, 3/*BLOCK_FIELD_IS_OBJECT*/);}

static void __NotUsePre__testBlockCopy_block_dispose_0(struct __NotUsePre__testBlockCopy_block_impl_0*src) {_Block_object_dispose((void*)src->objctTest, 3/*BLOCK_FIELD_IS_OBJECT*/);}

static struct __NotUsePre__testBlockCopy_block_desc_0 {
  size_t reserved;
  size_t Block_size;
  void (*copy)(struct __NotUsePre__testBlockCopy_block_impl_0*, struct __NotUsePre__testBlockCopy_block_impl_0*);
  void (*dispose)(struct __NotUsePre__testBlockCopy_block_impl_0*);
} __NotUsePre__testBlockCopy_block_desc_0_DATA = { 0, sizeof(struct __NotUsePre__testBlockCopy_block_impl_0), __NotUsePre__testBlockCopy_block_copy_0, __NotUsePre__testBlockCopy_block_dispose_0};

{% endcodeblock %}


{% codeblock lang:objc  带block修饰符的变量%}
static void __UseBlockPre__testBlockCopy_block_copy_0(struct __UseBlockPre__testBlockCopy_block_impl_0*dst, struct __UseBlockPre__testBlockCopy_block_impl_0*src) {_Block_object_assign((void*)&dst->i, (void*)src->i, 8/*BLOCK_FIELD_IS_BYREF*/);_Block_object_assign((void*)&dst->objctTest, (void*)src->objctTest, 8/*BLOCK_FIELD_IS_BYREF*/);}

static void __UseBlockPre__testBlockCopy_block_dispose_0(struct __UseBlockPre__testBlockCopy_block_impl_0*src) {_Block_object_dispose((void*)src->i, 8/*BLOCK_FIELD_IS_BYREF*/);_Block_object_dispose((void*)src->objctTest, 8/*BLOCK_FIELD_IS_BYREF*/);}

static struct __UseBlockPre__testBlockCopy_block_desc_0 {
  size_t reserved;
  size_t Block_size;
  void (*copy)(struct __UseBlockPre__testBlockCopy_block_impl_0*, struct __UseBlockPre__testBlockCopy_block_impl_0*);
  void (*dispose)(struct __UseBlockPre__testBlockCopy_block_impl_0*);
} __UseBlockPre__testBlockCopy_block_desc_0_DATA = { 0, sizeof(struct __UseBlockPre__testBlockCopy_block_impl_0), __UseBlockPre__testBlockCopy_block_copy_0, __UseBlockPre__testBlockCopy_block_dispose_0};
{% endcodeblock %}

通过前面几篇文章的学习，在_类名_函数名_block_impl_0 中用到的外部变量，会在stuct内部有一份与之同名的结构体变量，当这个变量内部包含isa时，系统会为他生成copy,和 dispose 函数，这点来看使用_ _blcok 和不用差别不大（注意：__UseBlockPre__testBlockCopy_block_copy_0 中访问的i已经不再是基础数据类型 int i,而是个 byref 的结构体）

那么blcok_copy是怎样调用的，内部是怎样实现的？前面我们遇到过一个例子，我们在把代码贴下

{% codeblock lang:objc %}
#import <UIKit/UIKit.h>
int main(int argc, char * argv[]) {
    @autoreleasepool {
        int i = 10;
        void (^blk)(void) = ^{i;};
        NSLog(@"直接打印一个block:%@ \n",^{i;});
        NSLog(@"打印赋值后的blk :%@ \n",blk);
        return 1;
    }
}
{% endcodeblock %}

结果
{% codeblock lang:objc %}
直接打印一个block:<__NSStackBlock__: 0x7ffee87aae70> 
打印赋值后的blk :<__NSMallocBlock__: 0x600003036370> 
{% endcodeblock %}
我们这里发现一个规律，普通的blcok的存储域是stack,一旦赋值给林外一个block ,这是新的block 的类型是mallco，这个过程其实发生的是block 的copy操作，但这种现象值发生在ARC环境下哦。
下面是会发生blcok 自动copy 的一些场景

|   场景名称         |是否带block |
| :----------    |:-----:|
| block 作为参数返回 |  ARC |
| block 赋值给 _ _strong 修饰的对象 | ARC |
| 在cocoa 中的api 含有usingblock | ARC |
| 作为GCD 的参数时 | ARC|

## copy 的调用流程
一般直接调用copy 方法对block 实行copy 操作，这个copy 函数的实现是在blcok 的desc 中，大致流程如下

```flow
st=>start: 开始执行copy
e=>end: 完成copy
op1=>operation: 调用block_desc_0 中的void (*copy)函数
op2=>operation: void (*copy)实际指向的是系统生成的 static void __类__函数_block_copy_0函数
op3=>operation: block_copy_0内部调用的是 _Block_object_assign 

st->op1->op2->op3->e
```
其实copy的关键代码都在里找到_Block_object_assign， 在opensource.apple.com 里找到_Block_object_assign 的具体实现
源码面前了无秘密

{% codeblock lang:js %}
void _Block_object_assign(void *destArg, const void *object, const int flags) {
    const void **dest = (const void **)destArg;
    switch (os_assumes(flags & BLOCK_ALL_COPY_DISPOSE_FLAGS)) {
      case BLOCK_FIELD_IS_OBJECT:
        /*******
        id object = ...;
        [^{ object; } copy];
        ********/

        _Block_retain_object(object);
        *dest = object;
        break;

      case BLOCK_FIELD_IS_BLOCK:
        /*******
        void (^object)(void) = ...;
        [^{ object; } copy];
        ********/

        *dest = _Block_copy(object);
        break;
    
      case BLOCK_FIELD_IS_BYREF | BLOCK_FIELD_IS_WEAK:
      case BLOCK_FIELD_IS_BYREF:
        /*******
         // copy the onstack __block container to the heap
         // Note this __weak is old GC-weak/MRC-unretained.
         // ARC-style __weak is handled by the copy helper directly.
         __block ... x;
         __weak __block ... x;
         [^{ x; } copy];
         ********/

        *dest = _Block_byref_copy(object);
        break;
        
      case BLOCK_BYREF_CALLER | BLOCK_FIELD_IS_OBJECT:
      case BLOCK_BYREF_CALLER | BLOCK_FIELD_IS_BLOCK:
        /*******
         // copy the actual field held in the __block container
         // Note this is MRC unretained __block only. 
         // ARC retained __block is handled by the copy helper directly.
         __block id object;
         __block void (^object)(void);
         [^{ object; } copy];
         ********/

        *dest = object;
        break;

      case BLOCK_BYREF_CALLER | BLOCK_FIELD_IS_OBJECT | BLOCK_FIELD_IS_WEAK:
      case BLOCK_BYREF_CALLER | BLOCK_FIELD_IS_BLOCK  | BLOCK_FIELD_IS_WEAK:
        /*******
         // copy the actual field held in the __block container
         // Note this __weak is old GC-weak/MRC-unretained.
         // ARC-style __weak is handled by the copy helper directly.
         __weak __block id object;
         __weak __block void (^object)(void);
         [^{ object; } copy];
         ********/

        *dest = object;
        break;

      default:
        break;
    }
}
{% endcodeblock %}


{% codeblock lang:objc %}
_Block_object_assign((void*)&dst->i, (void*)src->i, 8/*BLOCK_FIELD_IS_BYREF*/);_Block_object_assign((void*)&dst->objctTest, (void*)src->objctTest, 8/*BLOCK_FIELD_IS_BYREF*/);}
{% endcodeblock %}

上面是_Block_object_assign（） 参数传递的实际代码（从上面的代码段摘录），可以发现最后一个参数值是8 ，备注了BLOCK_FIELD_IS_BYREF
在_Block_object_assign 源码中执行的是一个switch Case ，Case BLOCK_FIELD_IS_BYREF 执行的函数是_Block_byref_copy


{% codeblock lang:objc %}
// A closure has been copied and its fixup routine is asking us to fix up the reference to the shared byref data
// Closures that aren't copied must still work, so everyone always accesses variables after dereferencing the forwarding ptr.
// We ask if the byref pointer that we know about has already been copied to the heap, and if so, increment and return it.
// Otherwise we need to copy it and update the stack forwarding pointer
static struct Block_byref *_Block_byref_copy(const void *arg) {
    struct Block_byref *src = (struct Block_byref *)arg;

    if ((src->forwarding->flags & BLOCK_REFCOUNT_MASK) == 0) {
        // src points to stack
        struct Block_byref *copy = (struct Block_byref *)malloc(src->size);
        copy->isa = NULL;
        // byref value 4 is logical refcount of 2: one for caller, one for stack
        copy->flags = src->flags | BLOCK_BYREF_NEEDS_FREE | 4;
        copy->forwarding = copy; // patch heap copy to point to itself
        src->forwarding = copy;  // patch stack to point to heap copy
        copy->size = src->size;

        if (src->flags & BLOCK_BYREF_HAS_COPY_DISPOSE) {
            // Trust copy helper to copy everything of interest
            // If more than one field shows up in a byref block this is wrong XXX
            struct Block_byref_2 *src2 = (struct Block_byref_2 *)(src+1);
            struct Block_byref_2 *copy2 = (struct Block_byref_2 *)(copy+1);
            copy2->byref_keep = src2->byref_keep;
            copy2->byref_destroy = src2->byref_destroy;

            if (src->flags & BLOCK_BYREF_LAYOUT_EXTENDED) {
                struct Block_byref_3 *src3 = (struct Block_byref_3 *)(src2+1);
                struct Block_byref_3 *copy3 = (struct Block_byref_3*)(copy2+1);
                copy3->layout = src3->layout;
            }

            (*src2->byref_keep)(copy, src);
        }
        else {
            // Bitwise copy.
            // This copy includes Block_byref_3, if any.
            memmove(copy+1, src+1, src->size - sizeof(*src));
        }
    }
    // already copied to heap
    else if ((src->forwarding->flags & BLOCK_BYREF_NEEDS_FREE) == BLOCK_BYREF_NEEDS_FREE) {
        latching_incr_int(&src->forwarding->flags);
    }
    
    return src->forwarding;
}
{% endcodeblock %}

flags 类型大致有以下几种，每种是

| 数据类型 | 说明 |实际调用函数|
| :----------:|:-----|:-----|
| BLOCK_FIELD_IS_OBJECT |  object对象 |_Block_retain_object |
| BLOCK_FIELD_IS_BLOCK  |  block 对象|_Block_copy |
| BLOCK_FIELD_IS_BYREF |   byref 对象|_Block_byref_copy |
| BLOCK_FIELD_IS_WEAK  | GC时代专用,已弃用|✘ |
| BLOCK_BYREF_CALLER  |  GC时代专用,已弃用 |✘|

### 参考资料 
https://opensource.apple.com/source/libclosure/libclosure-73/runtime.cpp.auto.html
