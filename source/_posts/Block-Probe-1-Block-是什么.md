---
title: Block Probe <一> Block 是什么
date: 2018-04-10 16:04:50
tags:
---
## Block 初接触

#### Block 的常见的用法

{% codeblock lang:objc %}
// block 声明和定义
returnType (^blockName)(parameterTypes) = ^returnType(parameters) {...};

// block 作为属性
@property (nonatomic, copy, nullability) returnType (^blockName)(parameterTypes);

//  block 作为参数
- (void)someMethodThatTakesABlock:(returnType (^nullability)(parameterTypes))blockName;
[someObject someMethodThatTakesABlock:^returnType (parameters) {...}];

// 作为C函数的参数
void SomeFunctionThatTakesABlock(returnType (^blockName)(parameterTypes));

//typedef 定义一个block 类型
typedef returnType (^TypeName)(parameterTypes);
TypeName blockName = ^returnType(parameters) {...};

{% endcodeblock %}

<!--more-->
#### 一个Block 实例讲解

{% asset_img blockExample.jpeg  图片来源网络 %}

block 有时候也被称作匿名函数，既然是函数那就有函数的声明和定义（创建）
"="符号 左侧的就是blcok 的声明，返回值类型 （^block名称）参数列表
"="符号 右侧的就是block的定义 ： ^ 返回值类型 参数列表 {表达式}

{% codeblock lang:objc %}
// 没有返回值，没有参数
^ void (void) {pritf("hello block \n");}

// 返回值可以省去
^ (void){pritf("hello block \n");}

// 没有参数的时候可以省去参数段 
^ {pritf("hello block \n");}
{% endcodeblock %}

## Block 的实现

#### 通过代码了解blcok 

从一段简单的代码开始：
{% codeblock lang:objc   %}
#import <stdio.h>
int main(int argc, char * argv[]) {
    int i = 10;
    void (^blk)(void) = ^{
    printf("%c",i);
    };
    blk();
    return 1;
}
{% endcodeblock %}

使用  "clang -rewrite-objc filename.m"命令 将mian.m 文件转换成mian.cpp 文件，在转换后代码中先找到
`int main(int argc, char * argv[])`,我们来看下他的代码实现：

{% codeblock lang:objc  %}
int main(int argc, char * argv[]) {
    int i = 10;
    // block 的声明和定义 ①
    void (*blk)(void) = ((void (*)())&__main_block_impl_0((void *)__main_block_func_0, &__main_block_desc_0_DATA, i));

    // blcok 的执行 ⑥
    ((void (*)(__block_impl *))((__block_impl *)blk)->FuncPtr)((__block_impl *)blk);
    return 1;
}
{% endcodeblock %}

↑ 这段代码有点小复杂，我们将其中的一些强制转换的部分去除，简化后的代码如下：

{% codeblock lang:objc  %}
int main(int argc, char * argv[]) {
    int i = 10;
    // block 的声明和定义 ①
    void (*blk)(void) = __main_block_impl_0(__main_block_func_0,__main_block_desc_0_DATA, i));

    // blcok 的执行 ②
    blk->FuncPtr(blk);
    return 1;
}
{% endcodeblock %}

↑ 在代码①中是将函数__main_block_impl_0 的执行结果返回给 blk；__main_block_impl_0 有三个参数__main_block_func_0，__main_block_desc_0_DATA，i，这三个参数下面将详细讲解。
  代码②是block的调用的过程，从这段代码能后发现block 的执行是函数地址的调用。

{% codeblock lang:c  %}
// block 结构体 ③
struct __main_block_impl_0 {
  struct __block_impl impl; // ④ 
  struct __main_block_desc_0* Desc; // ⑤ 
  int i;
  
  // ⑦ 构造函数 ，第一个参数fp,第二个参数desc，第三个参数flags 
  __main_block_impl_0(void *fp, struct __main_block_desc_0 *desc, int _i, int flags=0) : i(_i) {
    impl.isa = &_NSConcreteStackBlock;
    impl.Flags = flags;
    impl.FuncPtr = fp;
    Desc = desc;
  }
};

struct __block_impl {
  void *isa; // isa 指针，熟悉的味道
  int Flags; //  flags标记，在复杂的block 中会有作用
  int Reserved; // 保留标志位
  void *FuncPtr; //函数指针
};
{% endcodeblock %}
↑ 1，在搜索完整段代码后，发现__main_block_impl_0 一共出现两处，④处是个结构体，⑦ 是个和结构体同名的构造函数
 > c中的结构体是不可以有函数的，但是到了C++中的却可以声明一个函数，这个很意外吧？哈哈哈，swift 中的结构体也有类似功能

2，_ _main_block_impl 结构体有三个成员变量和一个构造函数组成，_ _block_impl，_ _main_block_desc_0，i(被捕获的对象)
  通过构造函数_ _main_block_impl_0 对结构内的成员结构体变量进行赋值，
  impl.Flags = flags;
  impl.FuncPtr = fp;
  Desc = desc;

上述代码的大致结构关系图
{% asset_img block-refrence.png Block Struct 结构图 %}

我们将成员变量从结构体中摘出来，可以看到main_block_impl_0 更清晰的结构：
{% codeblock lang:objc  %}
struct __main_block_impl_0 {
void *isa; // isa 指针，熟悉的味道
int Flags; //  flags标记，在复杂的block 中会有作用
int Reserved; // 保留标志位
void *FuncPtr; //函数指针
size_t reserved;
size_t Block_size;

__main_block_impl_0(void *fp, struct __main_block_desc_0 *desc, int _i, int flags=0) : i(_i){
isa = &_NSConcreteStackBlock;
Flags = flags;
FuncPtr = fp;
 Desc = desc;
}
{% endcodeblock %}

#### 真实的block

通过将oc代码转换成 C++代码后，我们了解了一个block 大致实现，但实际使用的block 会根据block内部捕获的变量的类型，变量的修饰符，配合runtime 和 编译器 动态生成blcok_Impl_0 的内容，我们可以通过runtime->Block_private 窥探更完整的block

{% codeblock lang:objc   %}
struct Block_layout {
    void *isa;
    volatile int flags; // contains ref count
    int reserved; 
    void (*invoke)(void *, ...);
    struct Block_descriptor_1 *descriptor;
    // imported variables
};

#define BLOCK_DESCRIPTOR_1 1
struct Block_descriptor_1 {
    unsigned long int reserved;
    unsigned long int size;
};

#define BLOCK_DESCRIPTOR_2 1
struct Block_descriptor_2 {
    // requires BLOCK_HAS_COPY_DISPOSE
    void (*copy)(void *dst, const void *src);
    void (*dispose)(const void *);
};

#define BLOCK_DESCRIPTOR_3 1
struct Block_descriptor_3 {
    // requires BLOCK_HAS_SIGNATURE
    const char *signature;
    const char *layout;
};
{% endcodeblock %}

{% asset_img block-struct.jpg Block Struct 结构图 %}

## Block 是什么

`书上给的定义`：带有自动变量的（局部变量）的匿名函数
通过本文的研究发现更准确的说法是：`block 是个OC对象，他包含了一个匿名函数和函数执行时需要的上下文`
* block中有isa ,所以它是个OC对象。
* block会根据内部使用的变量类型，变量修饰的关键字，对变量进行捕获处理
* block的执行其实是函数地址的调用，相关参数按照预定的规则保存到blcok的内部


## 参考资料
1 [谈Objective-C block的实现](https://blog.devtang.com/2013/07/28/a-look-inside-blocks/)
2 [Block技巧与底层解析](https://www.jianshu.com/p/51d04b7639f1)
3 [Block_private.h 源码](https://opensource.apple.com/source/libclosure/libclosure-67/Block_private.h.auto.html)
4 [runtime.c 源码](https://opensource.apple.com/source/libclosure/libclosure-38/runtime.c.auto.html)

