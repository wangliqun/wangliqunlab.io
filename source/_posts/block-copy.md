---
title: 'block <四>  [block copy]'
date: 2018-04-21 17:52:51
tags:
---
那么blcok_copy是怎样调用的，内部是怎样实现的？前面我们遇到过一个例子，我们在把代码贴下

{% codeblock lang:objc %}
#import <UIKit/UIKit.h>
int main(int argc, char * argv[]) {
    @autoreleasepool {
        int i = 10;
        void (^blk)(void) = ^{i;};
        NSLog(@"直接打印一个block:%@ \n",^{i;});
        NSLog(@"打印赋值后的blk :%@ \n",blk);
        return 1;
    }
}
{% endcodeblock %}

结果
{% codeblock lang:objc %}
直接打印一个block:<__NSStackBlock__: 0x7ffee87aae70> 
打印赋值后的blk :<__NSMallocBlock__: 0x600003036370> 
{% endcodeblock %}
我们这里发现一个规律，普通的blcok的存储域是stack,一旦赋值给林外一个block ,这是新的block 的类型是mallco，这个过程其实发生的是block 的copy操作，但这种现象值发生在ARC环境下哦。
<!--more-->
下面是会发生blcok 自动copy 的一些场景

|   场景名称         |是否带block |
| :----------    |:-----:|
| block 作为参数返回 |  ARC |
| block 赋值给 _ _strong 修饰的对象 | ARC |
| 在cocoa 中的api 含有usingblock | ARC |
| 作为GCD 的参数时 | ARC|

## copy 的调用流程
一般直接调用copy 方法对block 实行copy 操作，这个copy 函数的实现是在blcok 的desc 中，大致流程如下

```flow
st=>start: 开始执行copy
e=>end: 完成copy
op1=>operation: 调用block_desc_0 中的void (*copy)函数
op2=>operation: void (*copy)实际指向的是系统生成的 static void __类__函数_block_copy_0函数
op3=>operation: block_copy_0内部调用的是 _Block_object_assign 

st->op1->op2->op3->e
```
其实copy的关键代码都在里找到_Block_object_assign， 在opensource.apple.com 里找到_Block_object_assign 的具体实现
源码面前了无秘密

{% codeblock lang:js %}
void _Block_object_assign(void *destArg, const void *object, const int flags) {
    const void **dest = (const void **)destArg;
    switch (os_assumes(flags & BLOCK_ALL_COPY_DISPOSE_FLAGS)) {
      case BLOCK_FIELD_IS_OBJECT:
        /*******
        id object = ...;
        [^{ object; } copy];
        ********/

        _Block_retain_object(object);
        *dest = object;
        break;

      case BLOCK_FIELD_IS_BLOCK:
        /*******
        void (^object)(void) = ...;
        [^{ object; } copy];
        ********/

        *dest = _Block_copy(object);
        break;
    
      case BLOCK_FIELD_IS_BYREF | BLOCK_FIELD_IS_WEAK:
      case BLOCK_FIELD_IS_BYREF:
        /*******
         // copy the onstack __block container to the heap
         // Note this __weak is old GC-weak/MRC-unretained.
         // ARC-style __weak is handled by the copy helper directly.
         __block ... x;
         __weak __block ... x;
         [^{ x; } copy];
         ********/

        *dest = _Block_byref_copy(object);
        break;
        
      case BLOCK_BYREF_CALLER | BLOCK_FIELD_IS_OBJECT:
      case BLOCK_BYREF_CALLER | BLOCK_FIELD_IS_BLOCK:
        /*******
         // copy the actual field held in the __block container
         // Note this is MRC unretained __block only. 
         // ARC retained __block is handled by the copy helper directly.
         __block id object;
         __block void (^object)(void);
         [^{ object; } copy];
         ********/

        *dest = object;
        break;

      case BLOCK_BYREF_CALLER | BLOCK_FIELD_IS_OBJECT | BLOCK_FIELD_IS_WEAK:
      case BLOCK_BYREF_CALLER | BLOCK_FIELD_IS_BLOCK  | BLOCK_FIELD_IS_WEAK:
        /*******
         // copy the actual field held in the __block container
         // Note this __weak is old GC-weak/MRC-unretained.
         // ARC-style __weak is handled by the copy helper directly.
         __weak __block id object;
         __weak __block void (^object)(void);
         [^{ object; } copy];
         ********/

        *dest = object;
        break;

      default:
        break;
    }
}
{% endcodeblock %}


{% codeblock lang:objc %}
_Block_object_assign((void*)&dst->i, (void*)src->i, 8/*BLOCK_FIELD_IS_BYREF*/);_Block_object_assign((void*)&dst->objctTest, (void*)src->objctTest, 8/*BLOCK_FIELD_IS_BYREF*/);}
{% endcodeblock %}

上面是_Block_object_assign（） 参数传递的实际代码（从上面的代码段摘录），可以发现最后一个参数值是8 ，备注了BLOCK_FIELD_IS_BYREF
在_Block_object_assign 源码中执行的是一个switch Case ，Case BLOCK_FIELD_IS_BYREF 执行的函数是_Block_byref_copy


{% codeblock lang:objc %}
// A closure has been copied and its fixup routine is asking us to fix up the reference to the shared byref data
// Closures that aren't copied must still work, so everyone always accesses variables after dereferencing the forwarding ptr.
// We ask if the byref pointer that we know about has already been copied to the heap, and if so, increment and return it.
// Otherwise we need to copy it and update the stack forwarding pointer
static struct Block_byref *_Block_byref_copy(const void *arg) {
    struct Block_byref *src = (struct Block_byref *)arg;

    if ((src->forwarding->flags & BLOCK_REFCOUNT_MASK) == 0) {
        // src points to stack
        struct Block_byref *copy = (struct Block_byref *)malloc(src->size);
        copy->isa = NULL;
        // byref value 4 is logical refcount of 2: one for caller, one for stack
        copy->flags = src->flags | BLOCK_BYREF_NEEDS_FREE | 4;
        copy->forwarding = copy; // patch heap copy to point to itself
        src->forwarding = copy;  // patch stack to point to heap copy
        copy->size = src->size;

        if (src->flags & BLOCK_BYREF_HAS_COPY_DISPOSE) {
            // Trust copy helper to copy everything of interest
            // If more than one field shows up in a byref block this is wrong XXX
            struct Block_byref_2 *src2 = (struct Block_byref_2 *)(src+1);
            struct Block_byref_2 *copy2 = (struct Block_byref_2 *)(copy+1);
            copy2->byref_keep = src2->byref_keep;
            copy2->byref_destroy = src2->byref_destroy;

            if (src->flags & BLOCK_BYREF_LAYOUT_EXTENDED) {
                struct Block_byref_3 *src3 = (struct Block_byref_3 *)(src2+1);
                struct Block_byref_3 *copy3 = (struct Block_byref_3*)(copy2+1);
                copy3->layout = src3->layout;
            }

            (*src2->byref_keep)(copy, src);
        }
        else {
            // Bitwise copy.
            // This copy includes Block_byref_3, if any.
            memmove(copy+1, src+1, src->size - sizeof(*src));
        }
    }
    // already copied to heap
    else if ((src->forwarding->flags & BLOCK_BYREF_NEEDS_FREE) == BLOCK_BYREF_NEEDS_FREE) {
        latching_incr_int(&src->forwarding->flags);
    }
    
    return src->forwarding;
}
{% endcodeblock %}

flags 类型大致有以下几种，每种是

| 数据类型 | 说明 |实际调用函数|
| :----------:|:-----|:-----|
| BLOCK_FIELD_IS_OBJECT |  object对象 |_Block_retain_object |
| BLOCK_FIELD_IS_BLOCK  |  block 对象|_Block_copy |
| BLOCK_FIELD_IS_BYREF |   byref 对象|_Block_byref_copy |
| BLOCK_FIELD_IS_WEAK  | GC时代专用,已弃用|✘ |
| BLOCK_BYREF_CALLER  |  GC时代专用,已弃用 |✘|
