---
title: '漫谈 Objective-C  Property'
date: 2017-02-15 20:37:42
tags: iOS基础
---
###   property 基础概念
使用property 可以自动的生成属性的setter 和getter 方法，减少手写代码的数量
可以使用下面的格式声明一个property：
  @property (<#attributes#>) <#type#> <#name#> 
* @property 是keyword
* attributes 用来修饰property 可选的是 atomic,nonatomic,assign,strong,weak,copy 等

使用propery 在.h 进行声明后，编译器会在.m 文件中生成对应的代码 ，下面是个简单的例子
<!--more-->

{% codeblock lang:objc %}
@interface MyClass : NSObject
@property(copy) NSString *title;
@end
{% endcodeblock %}


{% codeblock lang:objc %}
@implementation MyClass
- (NSString *)title;
- (void)setTitle:(NSString *)newTitle;
@end
{% endcodeblock %}


{% blockquote %}
@synthesize title: 在早先的版本中使用 @synthesize关键字告诉编译器在编译的时候需要在.m 文件中生成setter 和getter 方法，**Xcode4将编译器从GCC换成了LLVM/ClANG 后，即使不写@synthesize ，编译器也会自动生成setter 和getter 方法**
{% endblockquote %}

{% blockquote %}
 @dynamic title: 告诉编译器在编译的时候没有发现title的访问方法也不必给出编译警告，他的访问方法可能在父类中实现，也可能在运行时动态生成。

 More info: ()
{% endblockquote %}
<!--more-->

### 常用的 attribute

#### atomic  &&  nonatomic
默认是atomic。

通过下面这个例子说明这两个关键字的区别
假设有属性 CGPoint P，有两个线程要对P同时进行操作
Thead A set p = CGPoint(1,1);
Thead B set p = CGPoint(2,2);

使用不同的关键字会的得到不同的结果
@property (atomic) CGPoint P;
这种情况下读到的结果p 可能是 （1，1）或者 （2，2）；
setter 方法的操作是原子的，能保证整个值得setter 过程时其他的线程是不可以操作这段代码的，但是最终结果是哪个不一定，后操作的线程或覆盖前面的结果
@property (nonatomic)   CGPoint P;
这种情况下读到的结果p 可能是 （1，1）或者 （2，2） 或者（1，2）或者（2，1），结果是难以预料的。

结论：
**atomic 是读写安全，线程不安全的。
nonatomic 读写，线程是都不安全的。**


附：
{% codeblock lang:c %}
static inline void reallySetProperty(id self, SEL _cmd, id newValue,
      ptrdiff_t offset, bool atomic, bool copy, bool mutableCopy)
    {
        id oldValue;
        id *slot = (id*) ((char*)self + offset);

        if (copy) {
            newValue = [newValue copyWithZone:NULL];
        } else if (mutableCopy) {
            newValue = [newValue mutableCopyWithZone:NULL];
        } else {
            if (*slot == newValue) return;
            newValue = objc_retain(newValue);
        }

        if (!atomic) {
            oldValue = *slot;
            *slot = newValue;
        } else {
            spin_lock_t *slotlock = &PropertyLocks[GOODHASH(slot)];
            _spin_lock(slotlock);
            oldValue = *slot;
            *slot = newValue;        
            _spin_unlock(slotlock);
        }

        objc_release(oldValue);
    }
{% endcodeblock %}

#### readwrite  && readonly
默认值是readwrite
通过该关键字告诉编译器生成哪些方法，
readwrite：  setter 和 getter 都会生成，
readonly： 只会生成 setter 方法

一个属性被readonly修饰时不可以被修改的，因为没有生成settr 方法。可以使用下面的方法在内部修改该属性值

{% codeblock lang:objc %}
	// .h 文件
	@interface AView : UIView
	@property (nonatomic,strong,readonly) NSString *firstName;

	//.m 文件
	@interface AView ()
	@property (nonatomic,strong,readwrite) NSString *firstName;
	@end
{% endcodeblock %}

#### strong && weak && assign && copy
默认值strong

| 名称       | owner 关系 | 引用关系 | retain count +1 |
|:-----------|:----------:|:--------:|:---------------:|
| assign     |     ✘      |    ✔     |        ✘        |
| weak       |     ✘      |    ✔     |        ✘        |
| strong     |     ✘      |    ✔     |        ✔        |
| copy（浅） |     ✘      |    ✔     |        ✘        |
| copy（深） |     ✔      |    ✘     |        ✘        |


**内存分区，内存主要分为以下几个区域**

1. 栈区（stack）: 编译器自动分配和释放，存放函数的参数值，局部变量（和数据结构中的栈不是一个概念）
2. 堆区 （heap）: 由程序员来分配释放，若程序员没有释放这些分配的区域则在结束时有系统回收，一般的内存泄露问题都是这个区域出现的问题。
3. 全局区 ： 全局变量和静态变量放在这块区域，初始化全局和静态变量放在一个区域，未初始化和全局和静态放在隔壁区域
4. 常量区： 常量字符串就是放在这个区域
5. 程序代码区： 二进制代码存放的区域

assign 用来修饰常量属性，属性的值一般储存于常量区，返回的是一个指向该常量的指针。常见的类型有float,int,double 等
用 weak 修饰的属性只会增加用关系，并不增加引用计数，一般delegate, IBOutlet等常用该关键字修饰们可以避免循环引用
copy 分浅copy和深copy ,浅copy是对指针地址的copy ,深copy是内存申请重新开辟一段空间，并将之前指针的内容拷贝到这块空间中。

> objective-c 中的内存管理其实也就是对分配在堆上的内存的管理，在引入ARC 机制后这块的管理变得更加简单，需要程序员做的就是使用适当的关键字来修饰当前属性，了解这些规则后能有效降低内存泄露，程序崩溃的概率。


### 参考

[1. Cocoa Core Competencies - Declared property](https://developer.apple.com/library/content/documentation/General/Conceptual/DevPedia-CocoaCore/DeclaredProperty.html)

[2. Dynamic Method Resolution](https://developer.apple.com/library/content/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtDynamicResolution.html)

[3. 深入浅出－iOS内存分配与分区](https://www.jianshu.com/p/7bbbe5d55440
)
